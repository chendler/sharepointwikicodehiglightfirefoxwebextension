
function getUnselectedText(allSelectedNodes) {
    var sel, range, tempRange, before = "", after = "";

	var firstSelectedNode = allSelectedNodes[0];
	var lastSelectedNode = allSelectedNodes[allSelectedNodes.length - 1];
	
    sel = window.getSelection();
    if (sel.rangeCount) {
        range = sel.getRangeAt(0);
    } else {
        range = document.createRange();
        range.collapse(true);
    }
    tempRange = document.createRange();

	if (range.startContainer === firstSelectedNode
		|| range.startContainer.compareDocumentPosition(firstSelectedNode) & Node.DOCUMENT_POSITION_PRECEDING
		|| range.startContainer.compareDocumentPosition(firstSelectedNode) & Node.DOCUMENT_POSITION_CONTAINS) {

		//tempRange.selectNodeContents(range.startContainer);
		tempRange.selectNodeContents(firstSelectedNode);
		tempRange.setEnd(range.startContainer, range.startOffset);
		before = tempRange.toString();
	}

	if (range.endContainer === lastSelectedNode
		|| range.endContainer.compareDocumentPosition(lastSelectedNode) & Node.DOCUMENT_POSITION_PRECEDING
		|| range.endContainer.compareDocumentPosition(lastSelectedNode) & Node.DOCUMENT_POSITION_CONTAINS) {

		//tempRange.selectNodeContents(range.endContainer);
		tempRange.selectNodeContents(lastSelectedNode);
		tempRange.setStart(range.endContainer, range.endOffset);
		after = tempRange.toString();
	}

    tempRange.detach();
	

    return {
        before: before,
		selection: sel.toString().length > range.toString().length ? sel.toString() : range.toString(),
        after: after
    };
}

function getSelectedNodes() {
	
	var sel = window.getSelection();
	var range = sel.getRangeAt(0);
	
	var allSelected = [];
	for (var i=0, el; el = range.commonAncestorContainer.childNodes[i]; i++) {
		if (sel.containsNode(el, true /*include partially selected*/) ) {
			allSelected.push(el);
		}
	}
	
	return allSelected;
}

function removeNodes(nodes) {
	for (var i=0, el; el = nodes[i]; i++) {
		nodes[i].parentNode.removeChild(nodes[i]);
	}
}

function insertNodesBefore(newNodes, referenceNode) {
	for (var i=0, el; el = newNodes[i]; i++) {
		referenceNode.parentNode.insertBefore(el, referenceNode);
	}
}

function getNewNodes(langName, unselectedText) {
	var nodes = [];
	var pre;

	if (unselectedText.before)
	{
		var s1 = document.createElement("span");
		s1.textContent = unselectedText.before;
		nodes.push(s1);
	}
	
	if (unselectedText.selection)
	{
		pre = document.createElement("pre");		
		var code = document.createElement("code");		
		code.className = langName;
		code.textContent = unselectedText.selection;
		pre.appendChild(code);
		nodes.push(pre);
	}
	
	if (unselectedText.after)
	{
		var s2 = document.createElement("span");
		s2.textContent = unselectedText.after;
		nodes.push(s2);
	}
	return {
		code: pre.childNodes[0],
		nodes: nodes
	};
}

function wrap(data) {
	var sel = window.getSelection();
	var range = sel.getRangeAt(0);

	var closestCode = range.commonAncestorContainer.closest("code");
	if (closestCode && (closestCode.parentNode.tagName == "PRE" || closestCode.parentNode.classList.contains('hljs'))) {
		closestCode = closestCode.parentNode;
	}

	var allSelected = closestCode ? [closestCode] : getSelectedNodes();
	var unselectedText = getUnselectedText(allSelected);

	var firstNode = allSelected[0];

	var codeNodes = getNewNodes(data.langName, unselectedText);	
	insertNodesBefore(codeNodes.nodes, firstNode);
		
	hljs.highlightBlock(codeNodes.code);

	removeNodes(allSelected);
}

function eatPage(request, sender, sendResponse) {
	switch (request.actionName) {
		case "wrap":
			wrap(request.data);
			break;
	}
}

chrome.runtime.onMessage.addListener(eatPage);