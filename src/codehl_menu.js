
console.log("chrome.i18n", chrome.i18n);
console.log("chrome.i18n.getMessagecontextMenuItemCodeHlHighlight", chrome.i18n.getMessage("contextMenuItemCodeHlHighlight"));

/*
Called when the item has been created, or when creation failed due to an error.
We'll just log success/failure here.
*/
function onCreated(n) {
  if (chrome.runtime.lastError) {
    console.log("error creating item:" + chrome.runtime.lastError);
  } else {
    console.log("item created successfully");
  }
}

var codeHlInsertMenuItemPrefix = "codeHlInsert";

chrome.contextMenus.create({
  id: "codeHlMenu",
  type: "normal",
  title: chrome.i18n.getMessage("contextMenuItemCodeHl"),
  contexts: ["editable"]
}, onCreated);

chrome.contextMenus.create({
  id: "codeHlInsert",
  parentId: "codeHlMenu",
  title: chrome.i18n.getMessage("contextMenuItemCodeHlHighlight"),
  contexts: ["all"]
}, onCreated);

chrome.contextMenus.create({
  id: "codeHlInsertJs",
  parentId: "codeHlInsert",
  title: chrome.i18n.getMessage("JavaScript"),
  contexts: ["all"]
}, onCreated);

chrome.contextMenus.create({
  id: "codeHlInsertCSharp",
  parentId: "codeHlInsert",
  title: chrome.i18n.getMessage("CSharp"),
  contexts: ["all"]
}, onCreated);

chrome.contextMenus.create({
  id: "codeHlInsertSQL",
  parentId: "codeHlInsert",
  title: chrome.i18n.getMessage("SQL"),
  contexts: ["all"]
}, onCreated);

chrome.contextMenus.create({
  id: "codeHlInsertPowerShell",
  parentId: "codeHlInsert",
  title: chrome.i18n.getMessage("PowerShell"),
  contexts: ["all"]
}, onCreated);

chrome.contextMenus.create({
  id: "codeHlInsertDOS",
  parentId: "codeHlInsert",
  title: chrome.i18n.getMessage("DOS"),
  contexts: ["all"]
}, onCreated);

chrome.contextMenus.onClicked.addListener(function(info, tab) {
	 
	if (info.menuItemId.startsWith(codeHlInsertMenuItemPrefix)) {
		var langName = info.menuItemId.substring(codeHlInsertMenuItemPrefix.length, info.menuItemId.length);
		console.log(langName);
		
		chrome.tabs.sendMessage(tab.id, {
			actionName: "wrap",
			data: {
				langName: langName
			}
		});
	}
});
